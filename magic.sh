#!/bin/bash
# This is an answer to the problem from the famous command line magic 
# from @climagic https://twitter.com/climagic/status/404992356513902592
grep -rh ^use --include="*.pl" --include="*.pm" . | sort | uniq -c 
