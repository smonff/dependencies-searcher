use strict;
use warnings;
use Data::Printer;
use feature qw(say);
use Dependencies::Searcher;

my $searcher = Dependencies::Searcher->new();
my @elements = $searcher->get_files();
#my $path = $searcher->build_full_path(@elements);

my @uses = $searcher->get_modules("^use", @elements);
#my @requires = $searcher->get_modules("^require", @elements);
my @requires;

my @merged_dependencies = $searcher->merge_dependencies(@uses, @requires);

my @real_modules = $searcher->avoid_superfluous(@merged_dependencies);

my @clean_modules = $searcher->clean_everything(@real_modules);
my @uniq_modules = $searcher->uniq(@clean_modules);

$searcher->dissociate(@uniq_modules);
$searcher->generate_report($searcher->non_core_modules);

