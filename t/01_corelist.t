use strict;
use warnings FATAL => 'all';
use Test::More tests => 1;
use feature qw(say);
use Module::CoreList;

# If it don't return 1, is_core is not available 
ok Module::CoreList::is_core("Module::CoreList"), "Module::CoreList must be at least 2.99";
