use strict;
use warnings FATAL => 'all';
use Test::More 'no_plan';
use Data::Printer;
use feature qw(say);
# This is not necessary, but a simple test, see Ovid's Book
use Dependencies::Searcher;

my $use_pattern = "use";
my $requires_pattern = "requires";

my $searcher = Dependencies::Searcher->new();
ok($searcher, "Searcher should be created");

can_ok($searcher, 'search');
can_ok($searcher, 'get_files');


my @paths = $searcher->get_files();

my @search_result_use = $searcher->search($use_pattern, @paths);
foreach (@search_result_use) {
  like( $_, qr/use/, "Array element contains a use");
  # do not contain any \t
  unlike( $_, qr/\\t/, "Array element don't contains a tab (\\t)" );
  # do not contain any \r
  unlike( $_, qr/\\r/, "Array element don't contains a carriage return (\\r)" );
  # is not a comment
  unlike( $_, qr/#/, "Array element don't seem to be a comment" );
  
  # Test we exclude the analyzed module namespace
  unlike( $_,
          qr/Dependencies::Searcher/, 
          "Array element don't contain analyzed module namespace" );
}


#my @ack_result_require = $searcher->search($cmd_require);
# Return nothing as long this distribution includes no requires
#ok(not(exists $ack_result_require[0]), 'This module should not return any requires');

#my @ack_results = (@ack_result_use, @ack_result_require);

