use strict;
use warnings FATAL => 'all';
use Test::More tests => 1;

use_ok('Dependencies::Searcher', "Use Dependencies::Searcher");

diag( "Testing Dependencies::Searcher $Dependencies::Searcher::VERSION, Perl $], $^X" );


