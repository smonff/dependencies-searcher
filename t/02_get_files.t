use strict;
use warnings FATAL => 'all';
use Test::More tests => 6;
use Data::Printer;
use feature qw(say);
use Dependencies::Searcher;


my $searcher = Dependencies::Searcher->new();
ok($searcher, "Searcher should be created");

can_ok($searcher, 'get_files');

my @paths = $searcher->get_files();

for my $path (@paths)  {
    if (-d $path) {
        isa_ok($path, "Path::Class::Dir");
    }

    if (-f $path) {
        isa_ok($path, "Path::Class::File");
    }
    # We don't want to have undefined paths in this array
    ok($path, "Path $path defined");
}


# returns the right paths
# returns a files sets in a directories tree
# get t/
# get Makefile.PL
