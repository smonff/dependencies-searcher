use strict;
use warnings;
use Test::More tests => 9;
use Dependencies::Searcher;
use Data::Printer;

# Tests that lines that matches the patterns don't contains
# unneccessary stuff after cleaning: it means stuff that are really
# non useful, not stuff that should be only cleaned because they
# contains too many elements etc.


my $searcher = Dependencies::Searcher->new();
can_ok($searcher, 'avoid_superfluous');


my @dirty_modules = (
    'use Fake::cat',
    'use feature qw(say)',
    'use 5.10',
    'use autodie',
    'use warnings',
    '1',
    'use Dependencies::Searcher',
    'or $module =~ m/^use\s[0-9]\.[0-9]+?/',
    #issue #76
    'use \s',
   );

my @cleaned_modules = $searcher->avoid_superfluous(@dirty_modules);

# The array should be "clean"
for my $module (@cleaned_modules) {
    unlike $module, qr/ say /xi, "Line should not contain any say feature";
    unlike $module, qr/ ^use\sautodie? /xi, "Line should not contain any autodie";
    unlike $module, qr/ ^use\swarnings? /xi, "Line should not contain any warnings";
    unlike $module, qr/ ^1$ /xi, "Line should not contain any '1'";
    unlike $module, qr/ ^use\sDependencies::Searcher /xi, "Line should not contain the name of it's own module";
    unlike $module, qr/ ^use\\s /xi, "Line should not contain any 'use\\s'";
    unlike $module, qr/ ^use\s\\s /xi, "Line should not contain any 'use \\s'";
}

# After removing a "Fake::Cat", the array should be empty because all the other ones will be cleaned
pop @cleaned_modules;
is pop @cleaned_modules, undef, "The array should be empty";


